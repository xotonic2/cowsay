#!/bin/bash
if [ $# -eq 0 ]; then
   /usr/games/fortune
else
   /usr/games/cowsay "$@"
fi
